﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MMTS_Live_Status.Models;
using MMTS_Live_Status.Resources;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;
using GoogleAds;

namespace MMTS_Live_Status
{
    public partial class MainPage : PhoneApplicationPage
    {
        public List<string> stationList { get; set; }
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
            Loaded += MainPage_Loaded;
            AssignDataToStationListPicker();
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            ToStationList.SelectionChanged += ToStationList_SelectionChanged;
            FromStationList.SelectionChanged += FromStationList_SelectionChanged;
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
            if (appSettings.Contains("from"))
            {
                string fromStation = (string)appSettings["from"];
                int index = stationList.IndexOf(fromStation);
                FromStationList.SelectedIndex = index;
            }
            else 
            {
                int index = stationList.IndexOf("Arts College");
                appSettings["from"] = "Arts College";
                FromStationList.SelectedIndex = index;
            }
            if (appSettings.Contains("to"))
            {
                string toStation = (string)appSettings["to"];
                int index = stationList.IndexOf(toStation);
                ToStationList.SelectedIndex = index;
            }
            else 
            {
                int index = stationList.IndexOf("Arts College");
                appSettings["to"] = "Arts College";
                ToStationList.SelectedIndex = index;
            }
        }        

        private void AssignDataToStationListPicker()
        {
            using (TrainDataContext ctx = new TrainDataContext(TrainDataContext.ConnectionString))
            {
                ctx.CreateIfNotExists();
                ctx.LogDebug = true;
                stationList = (from station in ctx.Station
                                            orderby station.StationName
                                            select station.StationName).ToList();
                FromStationList.ItemsSource = stationList;
                ToStationList.ItemsSource = stationList;
            }
        }

        private LiveTrain createTrainsFromData(string jsonData)
        {
            LiveTrain liveTrains = JsonConvert.DeserializeObject<LiveTrain>(jsonData);
            return liveTrains;
        }

        private LiveTrainModel mapObject(LiveTrain liveTrainFromApi, string route)
        {
            LiveTrainModel returningModel = new LiveTrainModel();
            List<Train> listOfTrains = new List<Train>();

            switch (route)
            {
                case "FL":
                    if (liveTrainFromApi.FL != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.FL)
                        {
                            Train tr = new Train();
                            tr.Route = "FL";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "LF":
                    if (liveTrainFromApi.LF != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.LF)
                        {
                            Train tr = new Train();
                            tr.Route = "LF";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "LH":
                    if (liveTrainFromApi.LH != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.LH)
                        {
                            Train tr = new Train();
                            tr.Route = "LH";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "HL":
                    if (liveTrainFromApi.HL != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.HL)
                        {
                            Train tr = new Train();
                            tr.Route = "HL";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "FH":
                    if (liveTrainFromApi.FH != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.FH)
                        {
                            Train tr = new Train();
                            tr.Route = "FH";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "HF":
                    if (liveTrainFromApi.HF != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.HF)
                        {
                            Train tr = new Train();
                            tr.Route = "HF";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                default:
                    returningModel.Trains = listOfTrains;
                    return returningModel;
            }
        }

        private async void LPIFM_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=0");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private async void FMLPI_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=1");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private async void LPIHYD_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=2");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private async void HYDLPI_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=3");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private async void HYDFM_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=4");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private async void FMHYD_OnTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            string navigationUri = string.Format("/ViewLiveTrains.xaml?code=5");
            NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.RelativeOrAbsolute));
        }

        private void FromStationList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
            if (!appSettings.Contains("from"))
            {
                appSettings.Add("from", e.AddedItems[0].ToString());
            }
            else
            {
                if(e.AddedItems.Count != 0)
                    appSettings["from"] = e.AddedItems[0].ToString();
            }
            string fromStation = (string)appSettings["from"];
            int index = stationList.IndexOf(fromStation);
            FromStationList.SelectedIndex = index;
            appSettings.Save();
        }

        private void ToStationList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
            if (!appSettings.Contains("to"))
            {
                appSettings.Add("to", e.AddedItems[0].ToString());
            }
            else
            {
                if(e.AddedItems.Count != 0)
                    appSettings["to"] = e.AddedItems[0].ToString();
            }
            string toStation = (string)appSettings["to"];
            int index = stationList.IndexOf(toStation);
            ToStationList.SelectedIndex = index;
            appSettings.Save();
        }

        private void Interchange_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            IsolatedStorageSettings appSettings = IsolatedStorageSettings.ApplicationSettings;
            string toTemp = (string)appSettings["to"];
            appSettings["to"] = (string)appSettings["from"];
            appSettings["from"] = toTemp;

            int toTempIndex = ToStationList.SelectedIndex;
            ToStationList.SelectedIndex = FromStationList.SelectedIndex;
            FromStationList.SelectedIndex = toTempIndex;
        }

        private void Search_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (FromStationList.SelectedIndex != ToStationList.SelectedIndex)
            {
                string navigationUri = string.Format("/SearchTrains.xaml?from={0}&to={1}", FromStationList.SelectedIndex, ToStationList.SelectedIndex);
                NavigationService.Navigate(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
            }
            else 
            {
                MessageBox.Show("Hey, I don't think you need a train for that distance. Walk and stay fit.");
            }
        }

        private void OnAdReceived(object sender, AdEventArgs e)
        {
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
        }
    }
}