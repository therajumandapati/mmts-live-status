﻿using MMTS_Live_Status.Models;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Data
{
    public class LocalDB : DataContext
    {
        public LocalDB(string connectionString) : base(connectionString)
        {
            
        }
        public Table<RunningTrain> Trains;
        public Table<Station> Stations;
    }
}
