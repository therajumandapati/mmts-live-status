﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using MMTS_Live_Status.Models;
using GoogleAds;

namespace MMTS_Live_Status
{
    public class TrainListSelector : DataTemplateSelector
    {
        public DataTemplate Sunday
        {
            get;
            set;
        }

        public DataTemplate NoSunday
        {
            get;
            set;
        }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FinalTrain foodItem = item as FinalTrain;
            if (foodItem != null)
            {
                if (foodItem.RunsOnSunday)
                {
                    return Sunday;
                }
                else
                {
                    return NoSunday;
                }
            }

            return base.SelectTemplate(item, container);
        }
    }

    public partial class SearchTrains : PhoneApplicationPage
    {
        public int from { get; set; }
        public int to { get; set; }
        public List<string> stationList { get; set; }
        public SundayTrains sundayTrains = new SundayTrains();
        public SearchTrains()
        {
            InitializeComponent();
            Loaded += SearchTrains_Loaded;
        }

        void SearchTrains_Loaded(object sender, RoutedEventArgs e)
        {
            FindTrains(from, to);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            from = Convert.ToInt32(NavigationContext.QueryString["from"]);
            to = Convert.ToInt32(NavigationContext.QueryString["to"]);
            using (TrainDataContext ctx = new TrainDataContext(TrainDataContext.ConnectionString))
            {
                stationList = (from station in ctx.Station
                               orderby station.StationName
                               select station.StationName).ToList();
            }
        }

        public void FindTrains(int fro, int to)
        {
            TrainListSelector.Visibility = Visibility.Collapsed;
            Loader.Visibility = Visibility.Visible;
            //LoaderAnimation.Begin();
            List<FinalTrain> runningTrains = new List<FinalTrain>();
            using (TrainDataContext context = new TrainDataContext(TrainDataContext.ConnectionString))
            {
                string fromStation = stationList[from];
                string toStation = stationList[to];
                FromStation.Text = fromStation;
                ToStation.Text = toStation;
                List<TrainDTO> trainsFromDb = (from train in context.RunningTrain
                                               select new TrainDTO
                                               {
                                                   Id = train.Id,
                                                   TrainNumber = train.TrainNumber,
                                                   ArrivalTime = Convert.ToDateTime(train.ArrivalTime),
                                                   StationName = train.StationName
                                               }).ToList();
                List<FinalTrain> trainsList = (from ftrains in trainsFromDb
                                         join ttrains in trainsFromDb on ftrains.TrainNumber equals ttrains.TrainNumber
                                         where ftrains.StationName == fromStation && ttrains.StationName == toStation
                                         where ftrains.Id < ttrains.Id
                                         select new FinalTrain 
                                         {
                                            TrainNumber = ftrains.TrainNumber,
                                            Departure = ftrains.ArrivalTime.TimeOfDay,
                                            Arrival = ttrains.ArrivalTime.TimeOfDay,
                                            RunsOnSunday = sundayTrains.ListOfTrains.Contains(ftrains.TrainNumber) ? false : true
                                         }).ToList();
                TrainListSelector.ItemsSource = trainsList.OrderBy(r => r.Departure).OrderBy(r => (r.Departure < DateTime.Now.TimeOfDay)).ToList();
            }
            //LoaderAnimation.Stop();
            Loader.Visibility = Visibility.Collapsed;
            TrainListSelector.Visibility = Visibility.Visible;
            FromToGrid.Visibility = Visibility.Visible;
            DataLoaded.Visibility = Visibility.Visible;
            
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.RelativeOrAbsolute));
        }

        private void TrainListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (TrainListSelector.SelectedItem == null)
                return;
            FinalTrain finalTrain = TrainListSelector.SelectedItem as FinalTrain;
            string navigationUrl = "/SingleTrain.xaml?trainNo=" + finalTrain.TrainNumber.Replace(" ", "");
            TrainListSelector.SelectedItem = null;
            NavigationService.Navigate(new Uri(navigationUrl, UriKind.RelativeOrAbsolute));
        }

        private void OnAdReceived(object sender, AdEventArgs e)
        {
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
        }
    }
}