﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    class Stop
    {
        public string Arrival { get; set; }
        public string StationName { get; set; }
    }
}
