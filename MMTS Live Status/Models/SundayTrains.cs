﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class SundayTrains
    {
        public List<string> ListOfTrains { get; set; }

        public SundayTrains() 
        {
            this.ListOfTrains = LoadData();
        }

        public List<string> LoadData() 
        {
            List<string> sundayTrains = new List<string>();
            sundayTrains.Add("47109");
            sundayTrains.Add("47110");
            sundayTrains.Add("47111");
            sundayTrains.Add("47112");
            sundayTrains.Add("47133");
            sundayTrains.Add("47134");
            sundayTrains.Add("47135");
            sundayTrains.Add("47136");
            sundayTrains.Add("47137");
            sundayTrains.Add("47158");
            sundayTrains.Add("47138");
            sundayTrains.Add("47160");
            sundayTrains.Add("47161");
            sundayTrains.Add("47179");
            sundayTrains.Add("47182");
            sundayTrains.Add("47184");
            sundayTrains.Add("47198");
            sundayTrains.Add("47199");
            sundayTrains.Add("47208");
            sundayTrains.Add("47209");
            return sundayTrains;
        }
    }
}
