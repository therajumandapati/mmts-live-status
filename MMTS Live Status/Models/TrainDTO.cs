﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class TrainDTO
    {
        public int Id { get; set; }
        public string TrainNumber { get; set; }
        public string StationName { get; set; }
        public DateTime ArrivalTime { get; set; }
    }
}
