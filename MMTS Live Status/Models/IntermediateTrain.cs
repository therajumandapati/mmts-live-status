﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    class IntermediateTrain
    {
        public int TrainNumber { get; set; }
        public string Departure { get; set; }
        public string DepartureStation { get; set; }
        public string Arrival { get; set; }
        public string ArrivalStation { get; set; }
    }
}
