﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    class SearchTrain
    {
        public int TrainNumber { get; set; }
        public List<Stop> Stops { get; set; }
    }
}
