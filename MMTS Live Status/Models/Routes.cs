﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class Routes
    {
        public string from { get; set; }
        public string to { get; set; }
    }
}
