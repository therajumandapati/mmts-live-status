﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class Train
    {
        public string Route { get; set; }
        public string No { get; set; }
        public string CurrentStation { get; set; }
        public string LastUpdated { get; set; }
    }
}
