﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class LiveTrain
    {
        public List<FL> FL { get; set; }
        public List<LF> LF { get; set; }
        public List<LH> LH { get; set; }
        public List<HL> HL { get; set; }
        public List<HF> HF { get; set; }
        public List<FH> FH { get; set; }
    }
    public class FL
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }

    public class LH
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }

    public class HF
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }

    public class FH
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }

    public class LF
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }

    public class HL
    {
        public string Trainno { get; set; }
        public string Station { get; set; }
    }
}
