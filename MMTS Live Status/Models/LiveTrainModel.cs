﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    public class LiveTrainModel
    {
        public List<Train> Trains { get; set; }
    }
}
