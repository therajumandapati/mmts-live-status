﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTS_Live_Status.Models
{
    class RunningTrain
    {
        public string TrainName { get; set; }
        public string StationName { get; set; }
        public string ArrivalTime { get; set; }
        public bool Sunday { get; set; }
    }
}
