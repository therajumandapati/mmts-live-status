﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using GoogleAds;

namespace MMTS_Live_Status
{
    public partial class SingleTrain : PhoneApplicationPage
    {
        private string trainNumber;

        public SingleTrain()
        {
            InitializeComponent();
            Loaded += SingleTrain_Loaded;
        }

        void SingleTrain_Loaded(object sender, RoutedEventArgs e)
        {
            ShowTrainData(trainNumber);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            trainNumber = NavigationContext.QueryString["trainNo"];
            TrainNumber.Text = "Train Number: " + trainNumber;
        }

        private void ShowTrainData(string trainNumber) 
        {
            if (!String.IsNullOrEmpty(trainNumber))
            {
                using (var context = new TrainDataContext(TrainDataContext.ConnectionString))
                {
                    var trainList = (from train in context.RunningTrain
                                     where train.TrainNumber.Replace(" ", "") == trainNumber
                                     select train).ToList();
                    if (trainList.Count != 0)
                        TrainList.ItemsSource = trainList;
                }
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.RelativeOrAbsolute));
        }

        private void OnAdReceived(object sender, AdEventArgs e)
        {
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
        }
    }
}