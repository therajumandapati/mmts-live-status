﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MMTS_Live_Status.Models;
using Newtonsoft.Json;
using GoogleAds;

namespace MMTS_Live_Status
{
    public partial class ViewLiveTrains : PhoneApplicationPage
    {
        private string routeCode;
        private string liveRoute;
        private LiveTrainModel liveTrains;
        private string littleRoute;
        public ViewLiveTrains()
        {
            InitializeComponent();
            Loaded += ViewLiveTrains_Loaded;
        }

        void ViewLiveTrains_Loaded(object sender, RoutedEventArgs e)
        {
            ShowRouteName(routeCode);
            GetLiveTrainData(routeCode);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            routeCode = NavigationContext.QueryString["code"];
        }

        private void ShowRouteName(string routeCode)
        {
            switch (routeCode)
            {
                case "0":
                    FromStation.Text = "LPI";
                    ToStation.Text = "FM";
                    return;
                case "1":
                    FromStation.Text = "FM";
                    ToStation.Text = "LPI";
                    return;
                case "2":
                    FromStation.Text = "LPI";
                    ToStation.Text = "HYD";
                    return;
                case "3":
                    FromStation.Text = "HYD";
                    ToStation.Text = "LPI";
                    return;
                case "4":
                    FromStation.Text = "HYD";
                    ToStation.Text = "FM";
                    return;
                case "5":
                    FromStation.Text = "FM";
                    ToStation.Text = "HYD";
                    return;
                default:
                    FromStation.Text = "ABC";
                    ToStation.Text = "XYZ";
                    return;
            }
        }

        private async void GetLiveTrainData(string routeCode)
        {
            NoTrainsFound.Visibility = Visibility.Collapsed;
            NoNetwork.Visibility = Visibility.Collapsed;
            LiveTrainListSelector.ItemsSource = null;
            LiveTrainListSelector.Visibility = Visibility.Collapsed;
            SpinningPanel.Visibility = Visibility.Visible;
            SpinningAnimation.Begin();
            //first get data from api
            string jsonData = await getDataFromApi();
            if (jsonData == null)
            {
                SpinningAnimation.Stop();
                SpinningPanel.Visibility = Visibility.Collapsed;
                NoNetwork.Visibility = Visibility.Visible;
                return;
            }
            LiveTrain live = createTrainsFromData(jsonData);
            string route = getRouteFromCode(routeCode);
            //show only relevant trains
            LiveTrainModel finalList = mapObject(live, route);
            if (finalList.Trains == null || finalList.Trains.Count == 0)
            {
                NoTrainsFound.Visibility = Visibility.Visible;
            }
            else
            {
                LiveTrainListSelector.ItemsSource = finalList.Trains;
                LiveTrainListSelector.Visibility = Visibility.Visible;
            }
            //add the last updated time stamp
            SpinningAnimation.Stop();
            SpinningPanel.Visibility = Visibility.Collapsed;
        }

        private static string getRouteFromCode(string code)
        {
            switch (code)
            {
                case "0":
                    return "LF";
                case "1":
                    return "FL";
                case "2":
                    return "LH";
                case "3":
                    return "HL";
                case "4":
                    return "HF";
                case "5":
                    return "FH";
                default:
                    return "LF";
            }
        }

        private static async Task<string> getDataFromApi()
        {
            try
            {
                HttpClient client = new HttpClient();
                string requestUri = "http://122.252.246.246:8081/MMTSLiveeng.html?cache=" + Guid.NewGuid().ToString();
                string jsonData;
                if (checkConnection())
                {
                    jsonData = await client.GetStringAsync(requestUri);
                }
                else
                {
                    jsonData = null;
                }
                return jsonData;
            }
            catch (Exception e) 
            {
                MessageBox.Show("Oops. Something happened. Please try again");
                return null;
            }
        }

        private static LiveTrain createTrainsFromData(string jsonData)
        {
            LiveTrain liveTrains = JsonConvert.DeserializeObject<LiveTrain>(jsonData);
            return liveTrains;
        }

        private static LiveTrainModel mapObject(LiveTrain liveTrainFromApi, string route)
        {
            LiveTrainModel returningModel = new LiveTrainModel();
            List<Train> listOfTrains = new List<Train>();

            switch (route)
            {
                case "FL":
                    if (liveTrainFromApi.FL != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.FL)
                        {
                            Train tr = new Train();
                            tr.Route = "FL";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                        returningModel.Trains = listOfTrains;
                    }
                    return returningModel;
                case "LF":
                    if (liveTrainFromApi.LF != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.LF)
                        {
                            Train tr = new Train();
                            tr.Route = "LF";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                        returningModel.Trains = listOfTrains;
                    }
                    return returningModel;
                case "LH":
                    if (liveTrainFromApi.LH != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.LH)
                        {
                            Train tr = new Train();
                            tr.Route = "LH";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                        returningModel.Trains = listOfTrains;
                    }
                    return returningModel;
                case "HL":
                    if (liveTrainFromApi.HL != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.HL)
                        {
                            Train tr = new Train();
                            tr.Route = "HL";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                    }
                    returningModel.Trains = listOfTrains;
                    return returningModel;
                case "FH":
                    if (liveTrainFromApi.FH != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.FH)
                        {
                            Train tr = new Train();
                            tr.Route = "FH";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                        returningModel.Trains = listOfTrains;
                    }
                    return returningModel;
                case "HF":
                    if (liveTrainFromApi.HF != null)
                    {
                        foreach (var singleTrain in liveTrainFromApi.HF)
                        {
                            Train tr = new Train();
                            tr.Route = "HF";
                            tr.No = singleTrain.Trainno;
                            tr.CurrentStation = singleTrain.Station;
                            tr.LastUpdated = DateTime.Now.ToString();
                            listOfTrains.Add(tr);
                        }
                        returningModel.Trains = listOfTrains;    
                    }
                    return returningModel;
                default:
                    returningModel.Trains = listOfTrains;
                    return returningModel;
            }
        }

        private void ApplicationBarIconButton_OnClick(object sender, EventArgs e)
        {
            if (routeCode != null)
            {
                GetLiveTrainData(routeCode);
            }
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.RelativeOrAbsolute));
        }

        private static bool checkConnection()
        {
            return Microsoft.Phone.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();
        }

        private void LiveTrainListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LiveTrainListSelector.SelectedItem == null)
                return;
            Train finalTrain = LiveTrainListSelector.SelectedItem as Train;
            string navigationUrl = "/SingleTrain.xaml?trainNo=" + finalTrain.No;
            LiveTrainListSelector.SelectedItem = null;
            NavigationService.Navigate(new Uri(navigationUrl, UriKind.RelativeOrAbsolute));
        }

        private void OnAdReceived(object sender, AdEventArgs e)
        {
        }

        private void OnFailedToReceiveAd(object sender, AdErrorEventArgs errorCode)
        {
        }
    }
}